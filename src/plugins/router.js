import Vue from 'vue'
import VueRouter from 'vue-router'
import Music from '../views/Music.vue'
import Contact from '../views/Contact.vue'
import Home from '../views/Home.vue'
import About from '../views/About.vue'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/music',
    name: 'Music',
    component: Music
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
]

const router = new VueRouter({
  routes,
  mode: "hash"
})

export default router
