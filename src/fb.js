import firebase from 'firebase' 
import 'firebase-firestore'
 
 
 // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyC5y8bFlB44axtfpf4OXPdgHesOzo1T6Y4",
    authDomain: "ghost-fba9f.firebaseapp.com",
    databaseURL: "https://ghost-fba9f.firebaseio.com",
    projectId: "ghost-fba9f",
    storageBucket: "ghost-fba9f.appspot.com",
    messagingSenderId: "612752481472",
    appId: "1:612752481472:web:a65b2e65219153de47e125",
    measurementId: "G-ZENHV4CKMC"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
  const db = firebase.firestore();

  DataView.settings({ timestamsInSnapshots: true })

  export default db;